<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home()
    {
        return view('home');
    }

    public function gustianisa_irman(Request $request)
    {
        $to = $request->to;

        return view('gustianisa-irman', [
            'title' => 'Gustianisa & Irman',
            'to' => $to
        ]);
    }
}
